import { Component, OnInit } from '@angular/core';
import {PropertyService} from "../services/property.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title: string = 'List of properties';
  lat: number = 19.37891;
  lng: number = -99.223507;
  markers = [];
  constructor(private propertyService: PropertyService) { }

  ngOnInit() {
    this.propertyService.getItems().subscribe(data => {
      console.log(data['rows'])
      for (let row of data['rows']) {
        this.markers.push({
          lat: parseFloat(row.lat),
          lng: parseFloat(row.lng),
          id: row.id,
          name: row.name,
          address: row.locality + ', '+ row.route + ', '+ row.political + ', '+ row.street_number
        });
      }
      this.avrgCoordinate();
    });
  }
  avrgCoordinate() {
    let lat = 0;
    let lng = 0;
    for (const mark of this.markers) {
      lat += mark.lat;
      lng += mark.lng;
    }
    this.lat = lat / this.markers.length;
    this.lng = lng / this.markers.length;
  }

}
