import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PropertyComponent } from './property/property.component';
import { ListComponent } from './property/list/list.component';
import { DetailComponent } from './property/detail/detail.component';
import { CreateComponent } from './property/create/create.component';
import { EditComponent } from './property/edit/edit.component';
const routes: Routes = [
  {   path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {   path: 'property', component: PropertyComponent, children: [
    {   path: '', component: ListComponent },
    {   path: 'create', component: CreateComponent},
    {   path: ':id/edit', component: EditComponent},
    {   path: ':id', component: DetailComponent},
  ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
