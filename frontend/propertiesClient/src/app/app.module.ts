import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AgmCoreModule } from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PropertyComponent } from './property/property.component';
import { HeaderComponent } from './header/header.component';
import { ListComponent } from './property/list/list.component';
import { CreateComponent } from './property/create/create.component';
import { EditComponent } from './property/edit/edit.component';
import { DetailComponent } from './property/detail/detail.component';

import {AddresPlaceService} from './services/addres-place.service';
import {AppService} from './app.service';
import {PropertyService} from './services/property.service';
import {FeatureService} from "./services/feature.service";
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PropertyComponent,
    HeaderComponent,
    ListComponent,
    CreateComponent,
    EditComponent,
    DetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDqSTafCCW9kJSMZgEsSGM4DsUcvRNCY4A',
      libraries: ['places']
    }),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AddresPlaceService,
    AppService,
    PropertyService,
    FeatureService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
