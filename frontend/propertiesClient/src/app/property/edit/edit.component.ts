import { Component, OnInit } from '@angular/core';
import {PropertyService} from "../../services/property.service";
import {ActivatedRoute, Params} from "@angular/router";
import {NgForm} from "@angular/forms";
import {FeatureService} from "../../services/feature.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  item: any;
  id: number;
  loaded: boolean;
  public localFeatures;
  constructor(private propertyService: PropertyService,
              private featureService: FeatureService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loaded = false;
    this.localFeatures = [];
    this.route.params.subscribe(
        (params: Params) => {
          this.id = +params["id"];
          this.propertyService.getItem(this.id).subscribe(data => {
            this.localFeatures = data['row'].features;
            this.item = {
              name: data['row'].name,
              code: data['row'].code,
              description: data['row'].description,
            };
            this.loaded = true;
          });
        }
    );
  }

  deleteFeature(id, index){
    this.featureService.deleteItem(id).subscribe((data)=>{
      this.localFeatures.splice(index, 1);
    })
  }

  createFeature(form: NgForm){
    let params = {
      features: [{name: form.value.featureName, description: form.value.featureDescription}],
      property_id: this.id
    };

    this.featureService.create(params).subscribe((data)=>{
      this.localFeatures.push({ name: form.value.featureName, description: form.value.featureDescription});
      form.reset();
    })
  }
  updateProperty(form: NgForm){
    this.propertyService.update(form.value, this.id).subscribe((data)=>{
      console.log('success');
    })
  }

}
