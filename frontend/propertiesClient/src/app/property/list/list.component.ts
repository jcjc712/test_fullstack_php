import { Component, OnInit } from '@angular/core';
import {PropertyService} from "../../services/property.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  markers = [];
  constructor(private propertyService: PropertyService) { }

  ngOnInit() {
    this.propertyService.getItems().subscribe(data => {
      this.markers = data['rows']
    });
  }

  delete(id, index){
    this.propertyService.deleteItem(id).subscribe(data => {
      this.markers.splice(index, 1);
    });
  }

}
