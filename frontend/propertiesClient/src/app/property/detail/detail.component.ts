import { Component, OnInit } from '@angular/core';
import {PropertyService} from "../../services/property.service";
import {ActivatedRoute, Params} from "@angular/router";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  item: any;
  id: number;
  loaded: boolean
  constructor(private propertyService: PropertyService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loaded = false;
    this.route.params.subscribe(
        (params: Params) => {
          this.id = +params["id"];
          this.propertyService.getItem(this.id).subscribe(data => {
            console.log(data['row']);
            this.item = data['row']
            this.loaded = true;
          });
        }
    );




  }
}
