import { Component } from '@angular/core';
import { ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import {AddresPlaceService} from '../../services/addres-place.service';
import {PropertyService} from "../../services/property.service";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  public localFeatures;

  @ViewChild('search')
  public searchElementRef: ElementRef;
  public finalAddress;
  public address;
  public selectedMarker:boolean;
  constructor(
      private mapsAPILoader: MapsAPILoader,
      private ngZone: NgZone,
      private addPlaceService: AddresPlaceService,
      private propertyService: PropertyService
  ) {}

  ngOnInit() {
    this.address = {political: '', establishment: ''};
    this.finalAddress = '';
    this.zoom = 16;
    this.latitude = 39.8282;
    this.longitude = -98.5795;
    this.selectedMarker = false;
    this.searchControl = new FormControl();
    this.localFeatures = [];

    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry == null) {
            return;
          }
          this.selectedMarker = true;
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.finalAddress = place.formatted_address;
          this.processAddress(place.address_components);
        });
      });
    });
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
      });
    }
  }
  public markPlace(event) {
    this.selectedMarker = true;
    const latlng = event.coords;
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.addPlaceService.getLatLan(latlng)
        .subscribe(
            result => {
              // needs to run inside zone to update the map
              this.finalAddress = result.formatted_address;
              this.processAddress(result.address_components);
            },
            error => console.log(error),
            () => console.log('Geocoding completed!')
        );
  }
  processAddress(result) {
    this.address = [];
    for (const el of result) {
      this.address[el.types[0]] = el.short_name;
    }
  }

  createProperty(form: NgForm){
    let params = form.value;
    params.features = this.localFeatures;
       this.propertyService.create(params).subscribe((data)=>{
      form.reset();
      this.localFeatures = [];
    })
  }

  deleteFeature(index){
    this.localFeatures.splice(index, 1);
  }

  createFeature(form: NgForm){
    this.localFeatures.push({ name: form.value.featureName, description: form.value.featureDescription});
    form.reset();
  }

}
