import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AppService} from "../app.service";

@Injectable()
export class FeatureService {

  private myHeaders;
  //private token;
  constructor(private http: HttpClient, private appService:AppService) {
    this.myHeaders = new HttpHeaders();
    this.myHeaders.set('Content-Type', 'application/json');
    this.myHeaders.set('Accept', 'application/json');
  }

  create(params){
    return this.http.post(this.appService.getServerDomain()+'/feature', params,{
      headers: this.myHeaders,
    });
  }
  deleteItem(id){
    return this.http.delete(this.appService.getServerDomain()+'/feature/'+id,{
      headers: this.myHeaders
    });
  }
}
