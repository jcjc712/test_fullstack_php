import { Injectable } from '@angular/core';
//import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import {Subject} from "rxjs/Subject";
import {AppService} from "../app.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class PropertyService {
  private localItems = [];
  private myHeaders;
  //private token;
  itemsChanged = new Subject<any>();
  constructor(private http: HttpClient, private appService:AppService) {
    this.myHeaders = new HttpHeaders();
    this.myHeaders.set('Content-Type', 'application/json');
    this.myHeaders.set('Accept', 'application/json');
  }
  create(params){
    return this.http.post(this.appService.getServerDomain()+'/property', params,{
      headers: this.myHeaders,
    });
  }
  update(params, id){
    return this.http.put(this.appService.getServerDomain()+'/property/'+id, params,{
      headers: this.myHeaders,
    });
  }
  getItems() {
    return this.http.get(this.appService.getServerDomain() + '/property', {
      headers: this.myHeaders
    });
  }
  getItem(id){
    return this.http.get(this.appService.getServerDomain()+'/property/'+id,{
      headers: this.myHeaders
    });
  }
  deleteItem(id){
    return this.http.delete(this.appService.getServerDomain()+'/property/'+id,{
      headers: this.myHeaders
    });
  }
}
