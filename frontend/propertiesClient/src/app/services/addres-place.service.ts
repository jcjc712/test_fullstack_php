import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
@Injectable()
export class AddresPlaceService {

  constructor() { }

  getLatLan(latlng) {
    const geocoder = new google.maps.Geocoder();
    return Observable.create(observer => {
      geocoder.geocode({'location': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[1]) {
            observer.next(results[1]);
            observer.complete();
          } else {
            console.log('No result found');
            observer.next({});
            observer.complete();
          }
        } else {
          console.log('Geocoder failed due to: ' + status);
          observer.next({});
          observer.complete();
        }
      });
    });
  }
}
