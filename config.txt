Configuración del examen practico.
-El backend fue desarrollado con el framework Laravel 5.5 (php 7.1) el cual me sirvió para hacer mis apis y
consumir las apis de Foresquare.
-El frontend está separado del backend ya que es un SPA, con Angular 5, aca instalé los un paquetes
con el que trabajé "Google Maps".
-Para el desarrollo en el back use el patrón de sideño Service-Repository.
-Ya no tuve tiempo para agregar la seguridad, esto lo hubiera hecho con JWT, igual les comparto
un reposotorio publico que tengo en mi github donde para un sistema manejo así el loggin, tambien lo
tengo montado en Heroku.
https://github.com/jcjc712/amazon-items-client
https://github.com/jcjc712/amazon-items-backend
-Y lo del CORS lo resuelvo con un middleware pero ya no me dió tiempo de agregarlo, por eso lo puse con
headers en in index.php.
-Les comparto el link de mi colección de Postman con la que probé mis apis locales.
https://www.getpostman.com/collections/0adace0b8039655a39b9


